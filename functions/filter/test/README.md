# Test Assets

`start.ics` is a raw calendar file.

`filtered_blacklist.ics` has been manually edited,
to remove events with `EMECA1MTA`.

`filtered_whitelist.ics` has been manually edited,
to include only events with `Martingales`.

`filtered_both_exclude.ics` includes only events which have "Martingales",
but excluding `EMECA1MTA` (i.e. include the second one, delete the first.)