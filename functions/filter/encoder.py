from json import JSONEncoder
from decimal import Decimal

# for json.dumps with Decimal
# https://stackoverflow.com/a/3885198/5443120
class DecimalEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return str(o)
        return super().default(o)
