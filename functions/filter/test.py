import json
import os
import unittest
from unittest.mock import patch, MagicMock
from time import time
from itertools import zip_longest
import os
from io import BytesIO
from decimal import Decimal
import json

from fastapi.testclient import TestClient
import boto3
from moto import mock_aws
from fastapi.testclient import TestClient
from icalendar import Calendar

import app
from encoder import DecimalEncoder

# test/filtered_blacklist.ics was manually filtered with this
blacklist_filter_entries = [
    'Industrial organization',
    'EMMAA1OPQ'
]


# test/filtered_whitelist.ics was manually filtered with this
whitelist_filter_entries = [
    "Martingales",
]


class TestBasic(unittest.TestCase):
    def setUp(self):
        with open('test/start.ics', 'r') as f:
            self.start_ics = f.read()

    def test_filter_events_neither(self):

        with open('test/unfiltered.ics', 'r') as f:
            expected_str = f.read()
        
        actual_str = app.filter_events(self.start_ics)

        expected_cal = Calendar.from_ical(expected_str)
        actual_cal = Calendar.from_ical(actual_str)
        self.assertEqual(expected_cal, expected_cal), "__eq__ not implemented properly"
        self.assertNotEqual(len(actual_cal.walk('VEVENT')), 0, f"All events filtered out")
        self.assertEqual(expected_cal, actual_cal)


    def test_filter_events_blacklist(self):
        with open('test/filtered_blacklist.ics', 'r') as f:
            expected_str = f.read()

        actual_str = app.filter_events(self.start_ics, bl=blacklist_filter_entries)

        expected_cal = Calendar.from_ical(expected_str)
        actual_cal = Calendar.from_ical(actual_str)

        self.assertNotEqual(len(actual_cal.walk('VEVENT')), 0, f"All events filtered out")
        for event in actual_cal.walk('VEVENT'):
            for b in blacklist_filter_entries:
                self.assertNotIn(b.lower(), event['SUMMARY'].lower())
                self.assertNotIn(b.lower(), event['DESCRIPTION'].lower())

        self.assertEqual(expected_cal, actual_cal)

    
    def test_filter_events_whitelist(self):

        with open('test/filtered_whitelist.ics', 'r') as f:
            expected_str = f.read()

        actual_str = app.filter_events(self.start_ics, wl=whitelist_filter_entries)

        expected_cal = Calendar.from_ical(expected_str)
        actual_cal = Calendar.from_ical(actual_str)

        self.assertNotEqual(len(actual_cal.walk('VEVENT')), 0, f"All events filtered out")
        for event in actual_cal.walk('VEVENT'):
            matched = False
            for w in whitelist_filter_entries:
                matched |= w.lower() in event['SUMMARY'].lower()
                matched |= w.lower() in event['DESCRIPTION'].lower()
            self.assertTrue(matched)

        self.assertEqual(expected_cal, actual_cal)

    

    
    def test_filter_events_both(self):

        with open('test/filtered_both.ics', 'r') as f:
            expected_str = f.read()
        
        actual_str = app.filter_events(self.start_ics, bl=blacklist_filter_entries, wl=whitelist_filter_entries)

        expected_cal = Calendar.from_ical(expected_str)
        actual_cal = Calendar.from_ical(actual_str)
        self.assertNotEqual(len(actual_cal.walk('VEVENT')), 0, f"All events filtered out")

        for event in actual_cal.walk('VEVENT'):
            matched = False
            for w in whitelist_filter_entries:
                matched |= w.lower() in event['SUMMARY'].lower()
                matched |= w.lower() in event['DESCRIPTION'].lower()
            self.assertTrue(matched)

            for b in blacklist_filter_entries:
                self.assertNotIn(b.lower(), event['SUMMARY'].lower())
                self.assertNotIn(b.lower(), event['DESCRIPTION'].lower())

        self.assertEqual(expected_cal, actual_cal)


    def test_decimal_json(self):
        json.dumps({'d': Decimal(1/3)}, cls=DecimalEncoder)

class Test(unittest.TestCase):
    mock_aws = mock_aws()

    headers = {
        'Content-Type': 'text/calendar',
        'Cache-Control': 'no-cache',
        'Custom-Header': 'blah blah'
    }

    def setUp(self):

        self.patcher = patch('urllib3.PoolManager.request')
        self.mock_request = self.patcher.start()

        os.environ["AWS_ACCESS_KEY_ID"] = "testing"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
        os.environ["AWS_SECURITY_TOKEN"] = "testing"
        os.environ["AWS_SESSION_TOKEN"] = "testing"
        os.environ["AWS_DEFAULT_REGION"] = "eu-west-3"

        self.mock_aws.start()
        app.s3_client = boto3.client('s3')
        app.s3_lambda = boto3.client('lambda')
        app.dynamodb = boto3.resource('dynamodb')

        os.environ['DOWNLOAD_LAMBDA'] = 'other-lambda'
        os.environ['TABLE_NAME'] = 'fake-table'
        os.environ['BUCKET_NAME'] = 'fake-bucket'
        os.environ['OBJECT_PREFIX'] = 'ics/'
        os.environ['DDB_EXPIRY_DAYS'] = '1'

        ddb_client = boto3.client('dynamodb')
        ddb_client.create_table(
            AttributeDefinitions=[
                {
                    'AttributeName': 'id',
                    'AttributeType': 'S'
                },
            ],
            TableName=os.environ['TABLE_NAME'],
            KeySchema=[
                {
                    'AttributeName': 'id',
                    'KeyType': 'HASH'
                },
            ],
            BillingMode='PAY_PER_REQUEST',
        )
        dynamodb = boto3.resource('dynamodb')
        self.table = dynamodb.Table(os.environ['TABLE_NAME'])

        s3 = boto3.resource('s3')
        self.bucket = s3.Bucket(os.environ['BUCKET_NAME'])
        self.bucket.create(
            CreateBucketConfiguration={
                'LocationConstraint': os.environ["AWS_DEFAULT_REGION"],
            },
        )
        # turned on for testing so we can see if duplicate files were saved
        self.bucket.Versioning().enable()

        self.test_client = TestClient(app.app)

        self.mock_request.return_value.status = 200
        with open('test/start.ics', 'rb') as f:
            self.mock_request.return_value.data = f.read()

        self.mock_function = patch('app.invoke_lambda', new_callable=MagicMock)
        self.mocked_function = self.mock_function.start()
        self.mocked_function.side_effect = self.mock_invoke_lambda


    def tearDown(self):
        self.mock_function.stop()
        self.mock_aws.stop()
        self.patcher.stop()
        
    def mock_invoke_lambda(self, id, s3_path):
        with open('test/start.ics', 'rb') as f:
            self.bucket.Object(s3_path).put(
                Body=f,
                Metadata=self.headers)
        return {
            'StatusCode': 200,
            'Payload': BytesIO(),
        }

    def test_patching(self):
        with app.PoolManager() as http:
            resp = http.request('GET', "https://example.com")
            self.assertEqual(resp.status, 200)

        with open('test/start.ics', 'rb') as f:
            expected = f.read()

        self.assertEqual(resp.data, expected, f"Response body not what we expected")

    def test_get_from_s3(self):
        id = 'abc'
        (bucket, key) = app.get_bucket_location(id)
        (body, headers) = app.get_from_s3(bucket, key)
        self.assertIsNone(body)

        data_rows = ["123", "qwer"]

        self.bucket.Object(key).put(Body='\n'.join(data_rows).encode())
        (body, headers) = app.get_from_s3(bucket, key)
        self.assertIsNotNone(body)
        for (actual, expected) in zip_longest(body.decode().split('\n'), data_rows):
            self.assertEqual(actual.rstrip('\n'), expected)

    def test_get_bucket_location(self):
        bucket, path = app.get_bucket_location('abc')
        self.assertEqual(bucket, self.bucket.name)


    def test_nofilter(self):
        params = {}
        self._test_case(params, 'test/unfiltered.ics')

    def test_whitelist(self):
        params = {
            'wl': whitelist_filter_entries
        }
        self._test_case(params, 'test/filtered_whitelist.ics')

    def test_blacklist(self):
        params = {
            'bl': blacklist_filter_entries
        }
        self._test_case(params, 'test/filtered_blacklist.ics')

    def test_both(self):
        params = {
            'bl': blacklist_filter_entries,
            'wl': whitelist_filter_entries
        }
        self._test_case(params, 'test/filtered_both.ics')

        os.environ['PRINT_OUTPUT'] = '1'
        self._test_case(params, 'test/filtered_both.ics')

    def _test_case(self, params, expected_path):

        id = 'abc123'

        response = self.test_client.get(f"/ics/v1/{id}", params=params)
        self.assertEqual(response.status_code,  200)
        
        with open(expected_path, 'r') as f:
            expected_cal = Calendar.from_ical(f.read())
        
        self.assertFalse(response.text.startswith('"'), "response was escaped when it shouldn't be")

        actual_cal = Calendar.from_ical(response.read())
        self.assertEqual(actual_cal, expected_cal)

        for (header_name, expected) in self.headers.items():
            if header_name != 'Content-Type':
                self.assertIn(header_name, response.headers, f"Missing header: {header_name}")
                self.assertEqual(response.headers[header_name], expected, f"Wrong value header for {header_name}")
        self.assertEqual(response.headers['Content-Type'], "text/calendar")

        # check what's in dynamodb
        row = self.table.get_item(Key={'id': id})['Item']
        self.assertGreater(row['ttl'], time())
        original_ttl = row['ttl']
        self.assertEqual(row["s3_path"], os.environ['OBJECT_PREFIX'].rstrip('/') + '/' + id + '.ics')


        # second run
        obj = self.bucket.Object(row["s3_path"])
        initial_version = obj.version_id
        response = self.test_client.get(f"/ics/v1/{id}", params=params)
        self.assertEqual(response.status_code, 200)

        # check there's no new version in S3
        # should have just read from the cache
        obj.reload()
        new_version = obj.version_id
        self.assertEqual(initial_version, new_version)

        # check ttl was updated
        row = self.table.get_item(Key={'id': id})['Item']
        self.assertGreater(row['ttl'], original_ttl)

    def test_bad(self):
        params = {
            'bl': blacklist_filter_entries,
            'wl': whitelist_filter_entries
        }
        id = 'null' # deliberately a Javascript Null into a string
        response = self.test_client.get(f"/ics/v1/{id}", params=params)
        self.assertEqual(response.status_code, 400)