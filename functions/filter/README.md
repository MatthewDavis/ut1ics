# Filter and serving Lambda

Takes in a payload, via a function URL.

Does the following:

* Check the URL starts with what we expect
* calculate MD5 hash
* decide S3 path from that
* put `id`, `s3_path`, `ttl` to dynamodb
* download file from S3. If not there yet, invoke other lambda, then download file
* filter the ICS file line by line, based on query parameters
* add a calendar name field, set to `TSE`
* return it

## Event payload input

The Lambda is invoked with a Function URL.
It looks at query parameters:

* `wl` (whitelist) - There can be multiple of these (`fe=a&fe=b`). These are the keywords in the subject to be included. If omitted, all entries are included.
* `bl` (blacklist) - Exclude events matching any of these. (Even if in `wl`.)

## Response body

```
{
   "statusCode": 200,
    "headers": {
        "Content-Type": "?"
    },
    "body": "ics file here",
    "isBase64Encoded": false
}
```