import urllib.request
from urllib3 import Retry, PoolManager
from io import StringIO, TextIOWrapper
from typing import Tuple, Iterable, Dict, Annotated, Union, TextIO, Optional, List
import json
import os
from time import time
from decimal import Decimal
import logging

from fastapi import FastAPI, Query, Response, HTTPException, status
from fastapi.responses import PlainTextResponse
from mangum import Mangum
import boto3
from aws_xray_sdk.core import patch_all
from icalendar import Calendar

from encoder import DecimalEncoder

H_PER_DAY = 24
SEC_PER_H = 60*60
SEC_PER_DAY=H_PER_DAY * SEC_PER_H


logger = logging.getLogger()
logger.setLevel("INFO")

if os.getenv('AWS_LAMBDA_FUNCTION_NAME'): # if deployed
    patch_all() # apply x-ray to global boto3 clients

# cache clients, to be faster
s3_client = boto3.client('s3')
lambda_client = boto3.client('lambda')
dynamodb = boto3.resource('dynamodb')

app = FastAPI()

wsgi = Mangum(app)
def lambda_handler(event, context=""):
    ret = wsgi(event=event, context=context)
    return ret

@app.get("/ics/v1/{id}")
def get_ics(id: str, wl: Annotated[list[str], Query()] = [], bl: Annotated[list[str], Query()] = []) -> str:
    logger.info(f"Received query for {id} {wl=} {bl=}")
    if id == 'null':
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="ID must be specified. (Choose from the dropdown menu)"
        )
    (bucket, key) = get_bucket_location(id)

    ics_start, headers = get_from_s3(bucket, key)
    if ics_start is None:
        invoke_lambda(id, key)
        ics_start, headers = get_from_s3(bucket, key)
        assert ics_start is not None

    ics_new = filter_events(ics_start, wl=wl, bl=bl)

    if os.getenv('PRINT_OUTPUT', '0').lower() in ['1', 'y', 'yes', 't', 'true']:
        logger.info("Returned ICS is:\n" + ics_new.decode())

    # only update DDB once we know the input is valid
    update_ddb(id, key)
    

    return Response(content=ics_new, media_type="text/calendar", headers=headers)


def update_ddb(id, s3_key):
    row = {
        'id': id,
        's3_path': s3_key,
        'ttl': Decimal(time()) + int(os.environ['DDB_EXPIRY_DAYS'])*SEC_PER_DAY
    }
    logger.info(f"Putting to ddb: {json.dumps(row, cls=DecimalEncoder)}")
    table = dynamodb.Table(os.environ['TABLE_NAME'])
    table.put_item(Item=row)

# returns [None, {}] if the file doesn't exist yet
def get_from_s3(bucket, key) -> [Optional[bytes], Dict[str, str]]:
    t_start = time()
    try:

        resp = s3_client.get_object(Bucket=bucket, Key=key)
    except s3_client.exceptions.NoSuchKey as ex:
        t_end = time()
        logger.info(f"S3 object s3://{bucket}/{key} does not exist (took {t_end - t_start:.1f}s)")
        return (None, {})
    else:
        t_end = time()
        logger.info(f"S3 object s3://{bucket}/{key} exists (took {t_end - t_start:.1f}s)")
        return (resp['Body'].read(), resp.get('Metadata', {}))

def invoke_lambda(id, s3_path):
    event = {
        "id": id,
        "s3_path": s3_path
    }
    logger.info(f"Invoking lambda {os.environ['DOWNLOAD_LAMBDA']} with {json.dumps(event)}")
    resp = lambda_client.invoke(
        FunctionName=os.environ['DOWNLOAD_LAMBDA'],
        InvocationType='RequestResponse',
        LogType='None',
        Payload=json.dumps(event).encode()
    )
    assert not resp.get('FunctionError'), f"Other lambda ({os.environ['DOWNLOAD_LAMBDA']}) failed: {resp.get('FunctionError')}"


# returns (bucket, path)
def get_bucket_location(id) -> Tuple[str, str]:
    bucket = os.environ['BUCKET_NAME']
    path = '/'.join([os.environ['OBJECT_PREFIX'].rstrip('/'), id + '.ics'])
    return (bucket, path)

def filter_events(ics: str, wl=[], bl=[], name='TSE', refresh_interval='PT12H') -> bytes:
    cal = Calendar.from_ical(ics)

    original_num_events = len(cal.walk('VEVENT'))
    if original_num_events == 0:
        logger.warning(f"No events in original calendar")

    # Use a slice copy to avoid issues with modifying the list during iteration
    if wl:
        for event in cal.walk('VEVENT')[:]:
        
            to_keep = False
            for w in wl:
                if w.lower() in event['SUMMARY'].lower():
                    to_keep |= True
                if w.lower() in event['DESCRIPTION'].lower():
                    to_keep |= True
            if not to_keep:
                cal.subcomponents.remove(event)

    if bl:
        for event in cal.walk('VEVENT')[:]:
            for b in bl:
                if b.lower() in event['SUMMARY'].lower():
                    logger.info(f"Removing event because {b=} in summary {event['SUMMARY']}")
                    cal.subcomponents.remove(event)
                elif b.lower() in event['DESCRIPTION'].lower():
                    logger.info(f"Removing event because {b=} in desc {event['DESCRIPTION']}")
                    cal.subcomponents.remove(event)
            

    cal['NAME'] = name
    cal['X-WR-CALNAME'] = name
    cal['REFRESH-INTERVAL;VALUE=DURATION'] =  refresh_interval

    new_num_events = len(cal.walk('VEVENT'))
    if new_num_events == 0:
        logger.warn(f"All {original_num_events} events filtered out")
    else:
        logger.info(f"{new_num_events}/{original_num_events} events remain after filtering")

    return cal.to_ical()
                
if __name__ == '__main__':
    logger.info(main()["body"])