# ICS File downloader

Receives event:

```
{
    "id": str,
    "s3_path": str
}
```

Downloads that URL (with retries),
and saves the result in S3.
Bucket: `os.environ['BUCKET_NAME']`,

Returns nothing.
