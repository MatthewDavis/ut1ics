import unittest
import os

import boto3
from moto import mock_aws

from app import lambda_handler 
import app

class Test(unittest.TestCase):
    @mock_aws
    def test(self):
        bucket_name = 'test-bucket'
        os.environ['BUCKET_NAME'] = bucket_name
        os.environ['URL_PREFIX'] = 'https://www.google.com'

        app.client = boto3.client('s3')
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_name)
        bucket.create(
            CreateBucketConfiguration={
                'LocationConstraint': ' 	eu-west-3'
            },
        )

        event = {
            "id": '?a=b',
            "s3_path": 'some/prefix'
        }

        lambda_handler(event)

        bucket.Object(event['s3_path']).get()['Body'].read()