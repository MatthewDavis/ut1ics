import urllib3
import os
import json
import logging

import boto3
from aws_xray_sdk.core import patch_all

if os.getenv('AWS_LAMBDA_FUNCTION_NAME'): # if deployed
    patch_all() # apply x-ray to global boto3 clients

http = urllib3.PoolManager(retries=8)
client = boto3.client('s3')


logger = logging.getLogger()
logger.setLevel("INFO")

def lambda_handler(event, context=None):
    logger.info(f"event={json.dumps(event)}")
    assert event['id'] != 'null', "Invalid id input"
    url = os.environ['URL_PREFIX'] + event['id']
    r = http.request('GET', url, preload_content=False)
    logger.info(json.dumps({"url_id": event['id'], "http_status": r.status}))
    if r.status != 200:
        logger.info(f"URL: {url}")
    assert r.status == 200, f"Bad status: {r.status}"
    client.upload_fileobj(r, os.environ['BUCKET_NAME'], event['s3_path'])
