import urllib.request
from urllib3 import Retry, PoolManager
from io import StringIO, TextIOWrapper
from typing import Tuple, Iterable

blacklist = [
    "Probability modelling",
    "Probability modeling",
    "Markov chains",
    "Environmental Economics",
    "Political economy",
    "Project management",
    "Understanding Real World Organizations",
    "Evolution of economic behaviour",
    "Markets and incentives",
    "EMECA1PD01", # prof dev 9:30
    "EMECA1PD02", # prof dev other 11:00
    "EMECA1PD03", # prof dev 9:30
    "EMECA11TD2", # monday morning theory of incentives tute
    "EMECA11TD1", # monday arvo game theory tute'
    "EMECA1FLE2", # advanced french
]

url = "https://ade-production.ut-capitole.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=8241fc38732002146a196abbaec7f5a1bd72d825015315fe66c60d53cab758dbf377b612dec2c5fba5147d40716acb136c03e67b339315cf"

def main(event={}, context=None):

    retries = Retry(status_forcelist=[404], status=100, backoff_factor=1.5)
    #with PoolManager(retries=retries) as http:
    with PoolManager() as http:
        raw = http.request('GET', url)
        assert raw.status == 200, f"Bad status: {raw.status}"
        with StringIO(raw.data.decode()) as fin:
            with StringIO('out.ical') as fout:
                line = fin.readline()
                assert line.rstrip() == 'BEGIN:VCALENDAR', f"Bad first line: {line}"
                
                while True:   
                    while line.rstrip() not in('BEGIN:VEVENT', 'END:VCALENDAR'):
                        fout.write(line)
                        line = fin.readline()
                    assert line != '', 'unexpected EOF'
                    if line.rstrip() == 'END:VCALENDAR':
                        fout.write(line)
                        assert fin.readline().strip() == ''
                        
                        payload = {
                            "statusCode": 200, 
                            "headers": {k:v for (k,v) in raw.headers.items()},
                            "body": fout.getvalue()
                        }
                        return payload
                    else:
                        assert line.rstrip() == 'BEGIN:VEVENT', f"bad line: {line}"
                        event = [line]
                        summary = None
                        description = None
                        reject = False
                        while line.rstrip() != 'END:VEVENT':
                            line = fin.readline()
                            event.append(line)
                            if line.startswith('SUMMARY:'):
                                assert summary is None, f"Duplicate summary. Was {summary}, new {line}"
                                summary = line.partition(':')[2].replace('\\n', '\n')
                            elif line.startswith('DESCRIPTION'):
                                assert description is None
                                description = line.partition(':')[2].replace('\\n', '\n')

                        assert summary is not None
                        summary = summary.lower()
                        for blackword in blacklist:
                            if blackword.lower() in summary:
                                reject |= True
                            if description and (blackword in description):
                                reject |= True
                        if not reject:
                            fout.write(''.join(event))
                        line = fin.readline()
                        
                    

                
if __name__ == '__main__':
    print(main()["body"])