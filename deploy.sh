#!/bin/bash
set -e
sam build
sam deploy --tags Project=tse-ics
aws cloudformation set-stack-policy --stack-name ics-filter --stack-policy-body file://stack_policy.json

echo Test: $(curl  -s -o /dev/null -w "%{http_code}" "https://vvq4ws26xtuivxfdmznouhi7ge0dpqcq.lambda-url.eu-west-3.on.aws/ics/v1/8241fc3873200214794657197a3a6ebee0fa50826f0818af216ea0a969d10dbfec7f554d6ed7ba1b8a72a25d105159e8dee3e8a66a2957ae?&wl=Fundamentals&wl=Valuing&wl=Sustainable%20Development&wl=Causal%20Inference&wl=Machine%20learning&wl=Agriculture&wl=Ethics&wl=M2%20EA%20EA" )
