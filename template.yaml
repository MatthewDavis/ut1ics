AWSTemplateFormatVersion: '2010-09-09'
Transform: 
  - 'AWS::LanguageExtensions'
  - AWS::Serverless-2016-10-31
Description: >
  ICS calendar filter
Parameters:
  EmailAddress:
    Description: Where to send email alerts
    Type: String
    Default: aws@mdavis.xyz

Globals:
  Function:
    Environment:
      Variables:
        BUCKET_NAME: !Ref Bucket
        OBJECT_PREFIX: ics/
        TABLE_NAME: !Ref Table
        URL_PREFIX: https://ade-production.ut-capitole.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=
    Architectures:
      - arm64
    Runtime: python3.11
    Tracing: Active

Resources:
  DownloadFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: functions/download/
      Handler: app.lambda_handler
      Timeout: 60
      Policies:
      - Version: '2012-10-17' 
        Statement:
          - Effect: Allow
            Action:
              - s3:PutObject
            Resource:
              - !Sub "${Bucket.Arn}/*"
  FilterFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: functions/filter/
      Handler: app.lambda_handler
      Environment:
        Variables:
          DOWNLOAD_LAMBDA: !Ref DownloadFunction
          DDB_EXPIRY_DAYS: 8
          PRINT_OUTPUT: 0
      Policies:
      - Version: '2012-10-17' 
        Statement:
          - Effect: Allow
            Action:
              - dynamodb:PutItem
            Resource:
              - !GetAtt Table.Arn
          - Effect: Allow
            Action:
              - s3:GetObject
            Resource:
              - !Sub "${Bucket.Arn}/*"
          - Effect: Allow
            Action:
              - s3:ListBucket
            Resource:
              - !GetAtt Bucket.Arn
          - Effect: Allow
            Action:
              - lambda:InvokeFunction
            Resource:
              - !GetAtt DownloadFunction.Arn
      FunctionUrlConfig:
        AuthType:  NONE
        Cors:
          AllowCredentials: False
          AllowHeaders: 
            - '*'
          AllowMethods: 
            - 'POST'
            - 'GET'
          AllowOrigins: 
            - '*'
      Timeout: 10
      ReservedConcurrentExecutions: 20

  FilterFunctionErrorAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmDescription: "ICS Filter lambda failed"
      MetricName: "Errors"
      Namespace: "AWS/Lambda"
      Statistic: "Sum"
      Dimensions:
        - Name: FunctionName
          Value: !Ref FilterFunction
      Period: 14400  # 4 hours in seconds
      EvaluationPeriods: 1
      Threshold: 4
      ComparisonOperator: "GreaterThanThreshold"
      AlarmActions:
        - !Ref AlarmTopic
      OKActions:  
        - !Ref AlarmTopic
      TreatMissingData: "notBreaching"

  ThrottleAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmDescription: "ICS Filter lambda is being throttled"
      MetricName: "Throttles"
      Namespace: "AWS/Lambda"
      Statistic: "Sum"
      Dimensions:
        - Name: FunctionName
          Value: !Ref FilterFunction
      Period: 600  # 10 minutes
      EvaluationPeriods: 1
      Threshold: 1 
      ComparisonOperator: "GreaterThanOrEqualToThreshold"
      AlarmActions:
        - !Ref AlarmTopic
      OKActions:  
        - !Ref AlarmTopic
      TreatMissingData: "notBreaching"


  AlarmTopic:
    Type: AWS::SNS::Topic
    Properties:
      TopicName: ics-filter-alarm

  EmailSubscription:
    Type: AWS::SNS::Subscription
    Properties:
      Endpoint: !Ref EmailAddress
      Protocol: email
      TopicArn: !Ref AlarmTopic


  Table:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions: 
        - AttributeName: id
          AttributeType: S
      KeySchema:
        - AttributeName: id
          KeyType: HASH

      BillingMode: PAY_PER_REQUEST
      TimeToLiveSpecification: 
        AttributeName: ttl
        Enabled: True


  StepFunc:
    Type: AWS::Serverless::StateMachine
    Properties:
      Definition:
        Comment: Invoke download lambda for each url in ddb
        StartAt: First Table Scan
        States:
          First Table Scan:
            Type: Task
            Parameters:
              TableName: !Ref Table
              #Limit: 2
              Select: SPECIFIC_ATTRIBUTES
              ProjectionExpression: "#id, #s3"
              ExpressionAttributeNames:
                "#id": "id"
                "#s3": "s3_path"
            Resource: arn:aws:states:::aws-sdk:dynamodb:scan
            Next: Map
          Map:
            Type: Map
            ItemProcessor:
              ProcessorConfig:
                Mode: INLINE
              StartAt: Invoke download lambda
              States:
                Invoke download lambda:
                  Type: Task
                  Resource: arn:aws:states:::lambda:invoke
                  OutputPath: "$.Payload"
                  Parameters:
                    Payload: 
                      id.$: "$.id.S"
                      s3_path.$: "$.s3_path.S"
                    FunctionName: !GetAtt DownloadFunction.Arn
                  Retry:
                  - ErrorEquals:
                    - AssertionError
                    IntervalSeconds: 5
                    MaxDelaySeconds: 900
                    MaxAttempts: 10
                    BackoffRate: 2
                  - ErrorEquals:
                    - Lambda.ServiceException
                    - Lambda.AWSLambdaException
                    - Lambda.SdkClientException
                    - Lambda.TooManyRequestsException
                    IntervalSeconds: 1
                    MaxAttempts: 3
                    BackoffRate: 2
                  End: true
            Next: Another page?
            ItemsPath: "$.Items"
            MaxConcurrency: 1
            ResultPath:
          Another page?:
            Type: Choice
            Choices:
            - And:
              - Variable: "$.LastEvaluatedKey"
                IsPresent: true
              - Not:
                  Variable: "$.LastEvaluatedKey"
                  IsNull: true
              Next: Scan next page
            Default: Success
          Success:
            Type: Succeed
          Scan next page:
            Type: Task
            Parameters:
              TableName: !Ref Table
              ExclusiveStartKey.$: "$.LastEvaluatedKey"
              #Limit: 2
              ProjectionExpression: "#id"
              ExpressionAttributeNames:
                "#id": "id"
            Resource: arn:aws:states:::aws-sdk:dynamodb:scan
            Next: Map
      Events:
        Cron:
          Properties:
            Description: cron
            ScheduleExpression: rate(6 hours) # if changing this, change the dashboard frequency
            State: ENABLED
          Type: ScheduleV2

      Policies:
      - Version: '2012-10-17' 
        Statement:
          - Effect: Allow
            Action:
              - lambda:InvokeFunction
            Resource:
              - !GetAtt DownloadFunction.Arn
          - Effect: Allow
            Action:
              - dynamodb:Scan
            Resource:
              - !GetAtt Table.Arn
      PropagateTags: True
      Tracing:
        Enabled: true
      
  Bucket:
    Type: AWS::S3::Bucket
    Properties:
      #BucketName: String
      LifecycleConfiguration:
        Rules:
          - Id: "DeleteAllAfterOneDay"
            Status: "Enabled"
            ExpirationInDays: 1
          - Id: "AbortMultipartUploadsAfterOneDay"
            Status: "Enabled"
            AbortIncompleteMultipartUpload:
              DaysAfterInitiation: 1
      #VersioningConfiguration:
      #  Status: "Enabled"

  Dashboard:
    Type: AWS::CloudWatch::Dashboard
    Properties:
      DashboardBody:
        Fn::ToJsonString:
          widgets:
          - height: 6
            width: 6
            y: 0
            x: 12
            type: metric
            properties:
              metrics:
              - - AWS/States
                - ExecutionsSucceeded
                - StateMachineArn
                - !Ref StepFunc
                - color: "#2ca02c"
                  label: Executions Succeeded
              - - "."
                - ExecutionsFailed
                - "."
                - "."
                - color: "#d62728"
                  label: Executions Failed
              view: timeSeries
              stacked: false
              region: !Ref AWS::Region
              stat: Sum
              period: 21600
              title: Step Function Executions
          - height: 6
            width: 6
            y: 0
            x: 6
            type: metric
            properties:
              metrics:
              - - AWS/States
                - ExecutionTime
                - StateMachineArn
                - !Ref StepFunc
                - stat: Average
                  label: Average
              - - "..."
                - label: Maximum
              - - "..."
                - stat: Minimum
                  label: Minimum
              view: timeSeries
              stacked: false
              region: !Ref AWS::Region
              stat: Maximum
              period: 21600
              title: Step Function Duration
          - height: 6
            width: 6
            y: 6
            x: 6
            type: metric
            properties:
              metrics:
              - - AWS/Lambda
                - Duration
                - FunctionName
                - !Ref DownloadFunction
                - label: Minimum
                  stat: Minimum
                  region: !Ref AWS::Region
              - - "..."
                - label: Average
                  stat: Average
                  region: !Ref AWS::Region
              - - "..."
                - label: Maximum
                  stat: Maximum
                  region: !Ref AWS::Region
              period: 21600
              region: !Ref AWS::Region
              view: timeSeries
              stacked: false
              title: Download Duration
          - height: 6
            width: 6
            y: 6
            x: 12
            type: metric
            properties:
              metrics:
              - - AWS/Lambda
                - Invocations
                - FunctionName
                - !Ref DownloadFunction
                - region: !Ref AWS::Region
              period: 21600
              region: !Ref AWS::Region
              title: Download Invocations
              view: timeSeries
              stacked: false
              stat: Sum
          - height: 6
            width: 6
            y: 6
            x: 0
            type: metric
            properties:
              metrics:
              - - AWS/Lambda
                - Errors
                - FunctionName
                - !Ref DownloadFunction
                - id: errors
                  color: "#d13212"
                  region: !Ref AWS::Region
              - - "."
                - Invocations
                - "."
                - "."
                - id: invocations
                  visible: false
                  region: !Ref AWS::Region
              - - expression: 100 - 100 * errors / MAX([errors, invocations])
                  label: Success rate (%)
                  id: availability
                  yAxis: right
                  region: !Ref AWS::Region
                  period: 21600
              period: 21600
              region: !Ref AWS::Region
              title: Download Errors
              yAxis:
                right:
                  max: 100
              view: timeSeries
              stacked: false
              stat: Sum
          - height: 6
            width: 6
            y: 0
            x: 0
            type: text
            properties:
              # no !Sub needed. Seems to be handled by the to-json thing
              markdown: |
                # Links

                [Step Function ${StepFunc.Name}](https://${AWS::Region}.console.aws.amazon.com/states/home?region=${AWS::Region}#/statemachines/view/arn%3Aaws%3Astates%3A${AWS::Region}%3A${AWS::AccountId}%3AstateMachine%3A${StepFunc.Name})

                [S3 Bucket ${Bucket}](https://s3.console.aws.amazon.com/s3/buckets/${Bucket}?region=${AWS::Region}&tab=objects)

                [DynamoDB Table ${Table}](https://${AWS::Region}.console.aws.amazon.com/dynamodbv2/home?region=${AWS::Region}#table?name=${Table})

                [Download Lambda ${DownloadFunction}](https://${AWS::Region}.console.aws.amazon.com/lambda/home?region=${AWS::Region}#/functions/${DownloadFunction}?tab=code)

                [Filter Lambda ${FilterFunction}](https://${AWS::Region}.console.aws.amazon.com/lambda/home?region=${AWS::Region}#/functions/${FilterFunction}?tab=code)

                [Git Repo](https://gitlab.com/MatthewDavis/ut1ics)
          - height: 6
            width: 6
            y: 12
            x: 0
            type: metric
            properties:
              metrics:
              - - AWS/Lambda
                - Errors
                - FunctionName
                - !Ref FilterFunction
                - id: errors
                  color: "#d13212"
                  region: !Ref AWS::Region
              - - "."
                - Invocations
                - "."
                - "."
                - id: invocations
                  visible: false
                  region: !Ref AWS::Region
              - - expression: 100 - 100 * errors / MAX([errors, invocations])
                  label: Success rate (%)
                  id: availability
                  yAxis: right
                  region: !Ref AWS::Region
                  period: 21600
              period: 21600
              region: !Ref AWS::Region
              title: Filter Errors
              yAxis:
                right:
                  max: 100
              view: timeSeries
              stacked: false
              stat: Sum
          - height: 6
            width: 6
            y: 12
            x: 6
            type: metric
            properties:
              metrics:
              - - AWS/Lambda
                - Duration
                - FunctionName
                - !Ref FilterFunction
                - label: Minimum
                  stat: Minimum
                  region: !Ref AWS::Region
              - - "..."
                - label: Average
                  stat: Average
                  region: !Ref AWS::Region
              - - "..."
                - label: Maximum
                  stat: Maximum
                  region: !Ref AWS::Region
              period: 21600
              region: !Ref AWS::Region
              view: timeSeries
              stacked: false
              title: Filter Duration
          - type: metric
            x: 12
            y: 12
            width: 6
            height: 6
            properties:
              metrics:
              - - AWS/Lambda
                - Invocations
                - FunctionName
                - !Ref FilterFunction
                - region: !Ref AWS::Region
              period: 21600
              region: !Ref AWS::Region
              title: Filter Invocations
              view: timeSeries
              stacked: false
              stat: Sum
      DashboardName: ICS_Filter
