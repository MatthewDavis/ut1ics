from urllib.parse import urlencode, urlunparse


raw_url = "https://ade-production.ut-capitole.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data=8241fc38732002146a196abbaec7f5a1bd72d825015315fe66c60d53cab758dbf377b612dec2c5fba5147d40716acb136c03e67b339315cf"
url_prefix = "https://ade-production.ut-capitole.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?data="

common_blacklist = [
    "industrial organization",
    "industrial organisation",
    "Advanced macroeconomics",
    "Development economics",
    "Martingales",
    "Food economics",
    "Panel data",
    "Behavioral and experimental economics",
    "Advanced microeconomics",
    "Data bases",
    "TSE Economist meetings",
    "TSE - Say it Aloud",
]

whitelists = {
    'Optimization': {
        'whitelist': [
            'Dynamic Optimization',
            'Dynamic Optimisation',
        ],
    },
    'Market Finance': {
        'whitelist': [
            'Market Finance'
        ]
    },
    'Corporate Finance': {
        'whitelist': [
            'Corporate Finance'
        ]
    },
    'Public Economics': {
        'whitelist': [
            'Public Economics'
        ],
        'blacklist': [
            "EMABA12TD1",
            "EMABA12TD2",
        ]
    },
    'Applied Econometrics': {
        'whitelist': [
            'Applied Econometrics'
        ],
        'blacklist': [
            "EMABA12TD1",
            "EMABA12TD2",
        ]
    },
    'Program Evaluation': {
        'whitelist': [
            'Program Evaluation'
        ],
        'blacklist': [
            'EMECA12TD1',
            'EMECA12TD2'
        ]
    },
    'Time Series': {
        'whitelist': [
            'Time Series',
            'Timeseries'
        ],
    },
    'Environmental': {
        'whitelist': [
            'Environmental'
        ],
    },
    'French': {
        'whitelist': [
            'FLE'
        ],
        'blacklist': [
            "EMECA1FLE2",
        ]
    },
}

def generate_url(whitelist, blacklist):

    assert raw_url.startswith(url_prefix)
    id = raw_url[len(url_prefix):]
    
    lambda_domain = 'vvq4ws26xtuivxfdmznouhi7ge0dpqcq.lambda-url.eu-west-3.on.aws'

    protocol = 'https'
    domain = lambda_domain
    basepath = f'/ics/v1/{id}'
    parameters = {'bl': blacklist, 'wl': whitelist}

    # Constructing the URL
    url_components = (protocol, domain, basepath, '', urlencode(parameters, doseq=True), '')
    url = urlunparse(url_components)
    return url


for (course, spec) in whitelists.items():
    blacklist = spec.get('blacklist', [])
    whitelist = spec['whitelist']
    url = generate_url(whitelist, blacklist)
    print(f"For {course}:\n{url}\n")

# now one for everything else
blacklist = common_blacklist
for (course, spec) in whitelists.items():
    blacklist.extend(spec.get('blacklist', []))
    blacklist.extend(spec.get('whitelist', []))
url = generate_url(whitelist, blacklist)
print(f"For remainder:\n{url}\n")

# one for all in one
blacklist = common_blacklist
for (course, spec) in whitelists.items():
    blacklist.extend(spec.get('blacklist', []))
url = generate_url(whitelist, blacklist)
print(f"For all in one:\n{url}\n")


print(f"Raw unfiltered URL:\n{raw_url}")
