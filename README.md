# ics-filter

This project deploys some Lambda functions to AWS,
to act as a middle man between the Toulouse School of Economics (TSE) Université Toulouse Capitole (UT1) calendar system,
and a calendar app on your phone, or Google Calendar.

It will filter out events based on keywords, so you don't see events for subjects you aren't taking, or tutorials that aren't yours.

The UT1 server is unbelievably unreliable. It fails more than 20% of the time.
This project will poll the server regularly and cache the results,
so that your phone's calendar app won't complain when it gets an ocassional 500 error.

## How to use it

I need to write this.

## Technical Implementation

### Dynamodb table

```
{
  "id": id, # the last part of the URL only
  "ttl": int # unix epoch time in seconds
  "s3_path": str # path within the S3 bucket for the file
}
```

## Development

Deploy the lambda with:

```
sam build && sam deploy
```

Update the stack policy with:

```
aws cloudformation set-stack-policy --stack-name ics-filter --stack-policy-body file://stack_policy.json
```